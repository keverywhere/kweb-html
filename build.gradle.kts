plugins {
    id("org.jetbrains.kotlin.js") version "1.4.10"
}

group = "com.gitlab.denissov.kweb-html"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
    jcenter()
}

kotlin {
    target {
        browser {
            testTask {
                useKarma {
                    useChromeHeadless()
                }
            }
        }
    }
}

dependencies {
    implementation(kotlin("stdlib-js"))
    implementation("org.jetbrains.kotlinx:kotlinx-html-js:0.7.2")
    testImplementation(kotlin("test-js"))
}
