import kotlinx.html.button
import kotlinx.html.classes
import kotlinx.html.dom.append
import kotlinx.html.h1
import kotlinx.html.js.onClickFunction
import kotlinx.html.p
import kotlinx.browser.window

val document = window.document
fun messageGenerator(name: String) = Helper().buildWelcomeMessage(name)

fun main() {
    val appRoot = document.querySelector("#app") ?: return
    appRoot.append {
        h1 {
            +messageGenerator("Max")
            classes = setOf("title")
        }
        p {
            button {
                +"Click me, please!"
                onClickFunction = {
                    window.alert("Thanks!")
                }
            }
        }
    }
}